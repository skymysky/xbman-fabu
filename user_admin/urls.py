"""user_admin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'useradmin.views.login'),
    url(r'^login/', 'useradmin.views.login'),
    url(r'^logout/', 'useradmin.views.logout'),
    url(r'^checkpasswork/', 'useradmin.views.checkpasswork'),
    url(r'^index/', 'useradmin.views.index'),
    url(r'^tables/', 'useradmin.views.tables'),
    url(r'^lvsmodify/', 'useradmin.views.lvsmodify'),
    url(r'^jenkinsmodify/', 'useradmin.views.jenkinsmodify'),
    url(r'^publishmodify/', 'useradmin.views.publishmodify'),
    url(r'^hostsmodify/', 'useradmin.views.hostshmodify'),
    url(r'^hostsoperate/', 'useradmin.views.hostsoperate'),
    url(r'^hostsreduction/', 'useradmin.views.hostsreduction'),
    url(r'^relemanager/', 'useradmin.views.relemanager'),
    url(r'^odline/', 'useradmin.views.odline'),
    url(r'^ipvsadm/', 'useradmin.views.ipvsadm'),
    url(r'^onrelease/', 'useradmin.views.onrelease'),
    url(r'^onlyrelease/', 'useradmin.views.onlyrelease'),
    url(r'^onlylvs/', 'useradmin.views.onlylvs'),
    url(r'^svnmarge/', 'useradmin.views.svnmarge'),
    url(r'^marge/', 'useradmin.views.magre'),
]
