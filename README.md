#xbman-fabu
XBMAN-Release 发布管理系统。

结合了Jenkins和lvs操作，实现操作LVS上下线，及发布功能。

###截图：
登陆

![登陆](http://git.oschina.net/weihaoxuan/images/raw/master/xbman_fabu/login.jpg "登陆")

首页

![首页](http://git.oschina.net/weihaoxuan/images/raw/master/xbman_fabu/index.jpg "首页")

发布页

![发布页](http://git.oschina.net/weihaoxuan/images/raw/master/xbman_fabu/fabu.jpg "发布页")

配置页

![配置页](http://git.oschina.net/weihaoxuan/images/raw/master/xbman_fabu/peizhi.jpg "配置页")

svn合并

![svn合并](http://git.oschina.net/weihaoxuan/images/raw/master/xbman_fabu/svn.jpg "svn合并")

部署方法：
    1、部署环境
        python2.7.10+django1.8.3
    2、开始部署
        nginx+uwsgi方式
        首先安装python2.7+pip （具体安装方式自行百度。）
        安装基本环境 pip install -r requrements.txt
        安装uwsgi  pip install uwsgi
        运行 uwsgi uwsgi.ini
        nginx和uwsgi结合（自行百度）
		
	3、登录说明
		默认关闭admin后台登录，系统默认登录账号密码为 admin/123456 登录后可自行修改